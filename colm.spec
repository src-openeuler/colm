Name:           colm
Version:        0.14.7
Release:        2
Summary:        Programming language designed for the analysis of computer languages

# aapl/ and some headers from src/ are the LGPLv2+
License:        MIT and LGPLv2+
URL:            https://www.colm.net/open-source/colm/
Source0:        https://www.colm.net/files/%{name}/%{name}-%{version}.tar.gz
Patch0:         change-config.patch
Patch1:         colm-libfsm-ac_check_lib.patch
Patch2:         Fix-colm_-m-coredump.patch

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  make
BuildRequires:  asciidoc

# Unfortunately, upstream doesn't exist and not possible to find version
Provides:       bundled(aapl)

%description
Colm is a programming language designed for the analysis and transformation
of computer languages. Colm is influenced primarily by TXL. It is
in the family of program transformation languages.

%package devel
Summary:        Development libraries and header files for %{name}
Requires:       %{name}%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description devel
%{summary}.

%prep
%autosetup -p1
# Do not pollute with docs
sed -i -e "/dist_doc_DATA/d" Makefile.am

%build
autoreconf -vfi
%configure --disable-static
%make_build

%install
%make_install
find %{buildroot}%{_libdir} -type f -name '*.la' -print -delete
install -p -m 0644 -D %{name}.vim %{buildroot}%{_datadir}/vim/vimfiles/syntax/%{name}.vim

%ldconfig_scriptlets

%files
%license COPYING
%doc README
%{_bindir}/%{name}*
%{_libdir}/lib%{name}-%{version}.so
%dir %{_datadir}/vim
%dir %{_datadir}/vim/vimfiles
%dir %{_datadir}/vim/vimfiles/syntax
%{_datadir}/vim/vimfiles/syntax/%{name}.vim
%{_datadir}/doc/%{name}/*
%{_datadir}/*.lm
%{_datadir}/runtests

%files devel
%{_libdir}/lib%{name}.so
%{_libdir}/libfsm*
%{_includedir}/%{name}/
%{_docdir}/%{name}/*
%{_includedir}/libfsm*
%{_includedir}/aapl*

%changelog
* Tue Apr 02 2024 wulei <wu_lei@hoperun.com> - 0.14.7-2
- Fix core dump

* Thu Sep 14 2023 xu_ping <707078654@qq.com> - 0.14.7-1
- Upgrade version to 0.14.7

* Tue Jun 23 2020 Yikun Jiang <yikunkero@gmail.com> - 0.13.0.7-1
- bump version to 0.13.0.7

* Sun Mar 29 2020 Wei Xiong <myeuler@163.com> - 0.14.0-1
- Package init
